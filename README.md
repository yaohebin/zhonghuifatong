# zhonghuifatong

## 安装依赖

```
npm install
```

## 开发

- server 模式开发：（自动打开浏览器，修改js和less文件后，自动刷新）
```
npm run server
```

- watch 监听文件模式开发：（修改js和css文件后，自动编译）
```
npm run watch
```

- 开发环境打包:（打包成开发环境代码）
```
npm run dev
```

- 生成环境打包:（打包成生产环境代码）
```
npm run build
```

## 查看打包后的站点

- 打包代码生成 dist 目录后，在根目录执行，可以访问打包后的文件
```
node websit.js
```

- 然后打开终端的网址可以浏览静态网页: `localhost:3000/view/index_index.html`
- 浏览其他网页则需要修改地址栏中的地址 `index_index.html` 为其他文件名
