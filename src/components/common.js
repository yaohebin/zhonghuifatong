$(function () {

  $(window).on('scroll', function () {

    if ($(window).width() > 768) {
      if ($(window).scrollTop() > 0) {
        $('#js-nav-header-wrap').addClass('bg-img');
      } else {
        $('#js-nav-header-wrap').removeClass('bg-img');
      }
    }
    if ($(window).scrollTop() >= $(window).height()) {
      $('#js-back-top').fadeIn();
    } else {
      $('#js-back-top').fadeOut();
    }
  });

  /*  */
  $('#js-banner-down').on('click', function () {
    $('html,body').animate({
      scrollTop: 800
    }, 800, 'swing');
  });

  /* 返回顶部 */
  $('#js-back-top').on('click', function () {
    $('html,body').animate({
      scrollTop: 0
    }, 500, 'swing');
  });

  /* 展开侧边隐藏 */
  $('#js-m-icon-menu').on('click', function () {
    if ($(this).find('.m-icon-close-menu').hasClass('hidden')) {
      $(this).find('.m-icon-close-menu').removeClass('hidden');
      $(this).find('.m-icon-menu').addClass('hidden');
      $('.m-aside-menu-wrap').addClass('show');
      $('.m-aside-menu-main').addClass('show');
      $(document.body).addClass('overflow');
    } else {
      $(this).find('.m-icon-close-menu').addClass('hidden');
      $(this).find('.m-icon-menu').removeClass('hidden');
      $('.m-aside-menu-wrap').removeClass('show');
      $('.m-aside-menu-main').removeClass('show');
      $(document.body).removeClass('overflow');
    }
  });

  /* 侧边隐藏 */
  $('.m-aside-menu-wrap').on('click', function () {
    $('.m-icon-close-menu').addClass('hidden');
    $('.m-icon-menu').removeClass('hidden');
    $('.m-aside-menu-wrap').removeClass('show');
    $('.m-aside-menu-main').removeClass('show');
    $(document.body).removeClass('overflow');
  });

  module.exports = {};
});