/* css模块 */
require('./index.less');

/* js模块  */
import {test} from '../../components/util.js';
import {} from '../../components/common.js';

$(() => {

  let locationHash = window.location.hash.slice(1);

  let contentActiveToggle = function (id) {
    $('#tab-content').find('.' +id).removeClass('hidden').siblings().addClass('hidden');
  };

  let switchId = function (id) {
    switch (id) {
      case 'company-intro':
        contentActiveToggle(id);
        break;
      case 'company-honor':
        contentActiveToggle(id);
        break;
      case 'company-news':
        contentActiveToggle(id);
        break;
      default:
        contentActiveToggle('company-honor');
        break;
    }
  };

  let tabActiveToggle = function (id) {
    if (id) {
      $('#tab-list').find('.article__hd-item').each(function () {
          $(this).removeClass('active');
          if ($(this).attr('data-id') === id) {
            $(this).addClass('active').siblings().removeClass('active');
          }
      });

      switchId(id);
      if ($(window).width() > 768) {
        $('html,body').animate({
          scrollTop: 710
        }, 500, 'swing');
      }
    }
  };

  if ($(window).width() > 768) {
    tabActiveToggle(locationHash);
  }

  $('.nav-header-wrap').find('.about-us-item').on('click', '.about-item', function () {
    let activeId = $(this).find('a').attr('data-id');
    tabActiveToggle(activeId);
  });

  $('#tab-list').on('click', '.article__hd-item', function () {
    let activeId = $(this).attr('data-id');
    tabActiveToggle(activeId);
  });
});