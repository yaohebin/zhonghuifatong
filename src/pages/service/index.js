/* css模块 */
require('./index.less');

/* js模块  */
import {test} from '../../components/util.js';
import {} from '../../components/common.js';

$(() => {
  let locationHash = window.location.hash.slice(1);

  let contentActiveToggle = function (id) {
    $('#tab-content').find('.' +id).removeClass('hidden').siblings().addClass('hidden');
  };

  let switchId = function (id) {
    switch (id) {
      case 'service-process':
        contentActiveToggle(id);
        break;
      case 'classic-case':
        contentActiveToggle(id);
        break;
      default:
        contentActiveToggle('service-process');
        break;
    }
  };

  let tabActiveToggle = function (id) {
    if (id) {
      $('#tab-list').find('.article__hd-item').each(function () {
        $(this).removeClass('active');
        if ($(this).attr('data-id') === id) {
          $(this).addClass('active').siblings().removeClass('active');
        }
      });

      switchId(id);
      if ($(window).width() > 768) {
        $('html,body').animate({
          scrollTop: 710
        }, 500, 'swing');
      }
    }
  };


  if ($(window).width() > 768) {
    tabActiveToggle(locationHash);
  }


  $('.nav-header-wrap').find('.service-case-item').on('click', '.service-item', function () {
    let activeId = $(this).find('a').attr('data-id');
    tabActiveToggle(activeId);
  });

  $('#tab-list').on('click', '.article__hd-item', function () {
    let activeId = $(this).attr('data-id');
    tabActiveToggle(activeId);
  });
});