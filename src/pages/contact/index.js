/* css模块 */
require('./index.less');

/* js模块  */
import {test} from '../../components/util.js';
import {} from '../../components/common.js';

$(function () {
  /*  */
  let map = new BMap.Map("company-address");
  map.addControl(new BMap.NavigationControl());
  let myGeo = new BMap.Geocoder();
  myGeo.getPoint("中汇法通（北京）知识产权服务有限公司", function(point) {
    if (point) {
      map.centerAndZoom(point, 18);
      let maker = new BMap.Marker(point);
      map.addOverlay(maker);
      map.enableScrollWheelZoom();
      map.openInfoWindow(new BMap.InfoWindow(
        "<h4 style='margin: 0; margin-bottom: 10px; font: 700 16px/20px Microsoft YaHei;'>中汇法通（北京）知识产权服务有限公司</h4>" +
        "<p style='color:#111111;font-size:12px;'><span>地址：</span>北京市朝阳区焦化路甲18号中国北京出版创意产业基地先导区一层105室</p>" +
        "<p style='color:#111111;font-size:12px;'><span>电话：</span>010-83631800</p>" +
        "<p style='color:#111111;font-size:12px;'><span>邮箱：</span>service@bjzhft.com</p>", {
          offset: new BMap.Size(0, -17)
        }), point);
    }
  });
});