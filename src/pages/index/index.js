/* css模块 */
require('./index.less');

/* js模块  */
import {test} from '../../components/util.js';
import {} from '../../components/common.js';

$(function () {
  let articleWrap = $('.article-wrap');
  if ($(window).width() <= 768) {
    let moreNewsUrl = articleWrap.find('.news-wrap').find('a.more').attr('href');
    articleWrap.find('.news-wrap').find('a.more').attr('href', moreNewsUrl.slice(0, moreNewsUrl.indexOf('#')) + '#m-company-news');

    let moreClassicCaseUrl = articleWrap.find('.success-case-wrap').find('a.more').attr('href');
    articleWrap.find('.success-case-wrap').find('a.more').attr('href', moreClassicCaseUrl.slice(0, moreClassicCaseUrl.indexOf('#')) + '#m-classic-case');
  }

  articleWrap.find('.service-area-wrap').find('.service-area__bd').find('.service-area-item').each(function () {
    let that = this;
    $(that).on('mouseenter', function () {
      $(that).siblings().find('a').removeClass('active');
      $(that).find('a').addClass('active');
    });
    $(that).on('mouseleave', function () {
      $(that).parent().find('a').removeClass('active');
      $(that).parent().find('.aways-active').addClass('active');
    });
  })
});